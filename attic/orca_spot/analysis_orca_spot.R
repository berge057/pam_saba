rm(list=ls())

library(caret)
library(alluvial)

path <- 'J:/PAM_saba/SABA01/orca_spot'

try(setwd(path),silent=TRUE)

HB_checked <- read.csv('SN276.1.20170305T113539Z.checked.txt',sep = '\t')
HB_checked        <- HB_checked[HB_checked$ID == 'HB' | HB_checked$ID == 'Possibly HB',]

HB_orca_spot <- read.csv('SN276.1.20170305T113539Z.orca_spot.txt',sep = '\t')
HB_orca_spot$bool   <- NA
HB_orca_spot$target <- 'noise'



for(idx_orca_spot in 1:dim(HB_orca_spot)[1]){
  print(idx_orca_spot/dim(HB_orca_spot)[1])
  
  flag_out    <- 0
  idx_checked <- 1
  
  while(idx_checked <= dim(HB_checked)[1] & flag_out == 0){
    Ia <- c(HB_checked[idx_checked,]$Begin.Time..s.,
            HB_checked[idx_checked,]$End.Time..s.)
    
    Ib <- c(HB_orca_spot[idx_orca_spot,]$Begin.time..s.,
            HB_orca_spot[idx_orca_spot,]$End.time..s.)
    
    if(!(Ib[1] > Ia[2] || Ia[1] > Ib[2])){
      HB_orca_spot$target[idx_orca_spot] <- 'orca'
      
      if(HB_orca_spot$Sound.type[idx_orca_spot] == 'noise'){
        HB_orca_spot$bool[idx_orca_spot] <- 0
      }else if(HB_orca_spot$Sound.type[idx_orca_spot] == 'orca'){
        HB_orca_spot$bool[idx_orca_spot] <- 1
      }
      flag_out <- 1
    }
    
    idx_checked <- idx_checked+1
  }
  
  if(HB_orca_spot$Sound.type[idx_orca_spot] == 'noise' & flag_out == 0){
    HB_orca_spot$bool[idx_orca_spot] <- 1
  }
}

# number of true positives
TP <- length(which(HB_orca_spot$Sound.type == 'orca' & HB_orca_spot$bool == 1))
# number of true negatives
TN <- length(which(HB_orca_spot$Sound.type == 'orca' & HB_orca_spot$bool == 0))
# number of false positives
FP <- length(which(HB_orca_spot$Sound.type == 'noise' & HB_orca_spot$bool == 1))
# number of false negatives
FN <- length(which(HB_orca_spot$Sound.type == 'noise' & HB_orca_spot$bool == 0))

# confusion matrix
cfm <- confusionMatrix(as.factor(HB_orca_spot$target),
                       HB_orca_spot$Sound.type)

# print all results. This is of course biased by the noise category which gives a lot of false negatives
print(cfm)

# a good start to understand confusion matrix: https://www.tutorialspoint.com/machine_learning_with_python/machine_learning_with_python_confusion_matrix.htm
# some ideas to plot the confusion matrix: https://github.com/topepo/caret/issues/755
# plot confusion matrix as tiles
mosaicplot(cfm$table)

# another way to plot the confusion matrix
plotCM <- function(cm){
  cmdf <- as.data.frame(cm[["table"]])
  cmdf[["color"]] <- ifelse(cmdf[[1]] == cmdf[[2]], "green", "red")
  
  alluvial::alluvial(cmdf[,1:2]
                     , freq = cmdf$Freq
                     , col = cmdf[["color"]]
                     , alpha = 0.5
                     , hide  = cmdf$Freq == 0
  )
}

plotCM(cfm)