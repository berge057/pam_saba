parse_prediction <- function(threshold,pred.name){
  con=file(pred.name,open="r")
  lines <- readLines(con)
  
  # find the start of parameter 2
  out <- grep(pattern='|I|time', lines,value=TRUE,fixed = TRUE)
  
  HB_orca_spot <- as.data.frame(array(data = NA,dim = c(length(out),4)))
  colnames(HB_orca_spot) <- c('time_start','time_end','prob','pred')
  
  for(idxLine in 1:length(out)){
    #print(idxLine/length(out))
    splitLine <- unlist(strsplit(out[idxLine],split=','))
    
    # fill in time
    timeStr <- unlist(strsplit(splitLine[1],split='time='))
    timeStr <- timeStr[2]
    timeStr <- unlist(strsplit(timeStr,split='-'))
    HB_orca_spot$time_start[idxLine] <- as.numeric(timeStr[1])
    HB_orca_spot$time_end[idxLine]   <- as.numeric(timeStr[2])
    
    # fill in probability
    prob <- unlist(strsplit(splitLine[3],split='prob='))
    HB_orca_spot$prob[idxLine] <- as.numeric(prob[length(prob)])
    
    # fill in prediction
    if(HB_orca_spot$prob[idxLine] > threshold){
      HB_orca_spot$pred[idxLine] <- 'HB'
    }else{
      HB_orca_spot$pred[idxLine] <- 'noise'
    }
  }
  
  close(con)
  
  return(HB_orca_spot)
}