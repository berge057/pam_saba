clear all
close all
clc

addpath(genpath(fullfile(pwd,'PamguardMatlab_20200113')))

mainPath = pwd;
dataPath = fullfile(pwd,'data');

% % parameters
samplerate_high     = 48000;
samplerate_low      = 6000;
fftlength_high      = 512;
fftlength_low       = 4096;

channel=0; %use channel zero (these data are form one channel anyway)
plotLTSA=true; %true to autimaticall plot the LTSA
hsens=-165;%hydrophone sensitivity in dB re 1V/uPa
vp2p=5; %peak to peak voltage in dB
gain=6; %gain in dB

thirdOctaveBand = readtable(fullfile(dataPath,'third_octave_bands.csv'),'Delimiter',',');
                
dataSet_mapping = readtable(fullfile(dataPath,'PAM_SABA_overview.csv'),'Delimiter',',');

uniqueStations = unique(dataSet_mapping.station_ID);

for idxStation = 1:length(uniqueStations)
    % process data from each station

    % setting paths
    stationPath     = fullfile(mainPath,uniqueStations(idxStation));
    pamguardPath    = fullfile(stationPath,'PAMguard');
    workspacePath   = fullfile(stationPath,'PAMguard','workspace');
    resultPath      = fullfile(stationPath,'PAMguard','results');
    figurePath      = fullfile(stationPath,'PAMguard','figures');

    % filtering table for data sets
    dataSet_mapping_station = dataSet_mapping(strcmp(dataSet_mapping.station_ID,uniqueStations(idxStation)),:);

    % load file mapping
    fileMappingTab = readtable(char(fullfile(stationPath,strcat(uniqueStations(idxStation),'_file_inventory_all.csv'))),'Delimiter',',');

    % % loop on each data set
    for idxDataSet = 1:size(dataSet_mapping_station,1)

        dataSetPath = char(fullfile(pamguardPath, dataSet_mapping_station.data_name(idxDataSet)));
        binaryPath  = char(fullfile(dataSetPath,'binary'));

    %     runName = char(matFileAndName{idxDataSet}(2));

        folderMat = dir(binaryPath);
        folderMat = folderMat(3:end);
        folderMat = folderMat([folderMat.isdir] == 1);

        for idxFolder = 1:length(folderMat)
            disp(strcat(dataSet_mapping_station.station_ID(idxDataSet),'-',folderMat(idxFolder).name))
            fileMat = dir(fullfile(binaryPath,folderMat(idxFolder).name,'*.pgdf'));

            % whistle and moan detector high freq
            disp('extract WhistlesMoans highFreq')
            process_WhistlesMoans(   ...
                                    fileMat, ...
                                    fileMappingTab, ...
                                    samplerate_high, ...
                                    fftlength_high, ...
                                    'WhistlesMoans_Whistle_and_Moan_Detector_highFreq_Contours', ...
                                    char(strcat(folderMat(idxFolder).name,'_WhistlesMoans_highFreq_',dataSet_mapping_station.data_name(idxDataSet))), ...
                                    'highFreq', ...
                                    fullfile(workspacePath,'WhistlesMoans_highFreq'), ...
                                    fullfile(resultPath,'selection_tables_highFreq'));

            % whistle and moan detector low freq
            disp('extract WhistlesMoans lowFreq')
            process_WhistlesMoans(  ...
                                    fileMat, ...
                                    fileMappingTab, ...
                                    samplerate_low, ...
                                    fftlength_low, ...
                                    'WhistlesMoans_Whistle_and_Moan_Detector_lowFreq_Contours', ...
                                    char(strcat(folderMat(idxFolder).name,'_WhistlesMoans_lowFreq_',dataSet_mapping_station.data_name(idxDataSet))), ...
                                    'lowFreq', ...
                                    fullfile(workspacePath,'WhistlesMoans_lowFreq'), ...
                                    fullfile(resultPath,'selection_tables_lowFreq'));

            % noise monitor
            disp('extract NoiseMonitor')
            process_NoiseMonitor(   fileMat, ...
                                    'Noise_Band_Noise_Band_Monitor_Noise_Band_Monitor', ...
                                    fullfile(workspacePath,'NoiseMonitor'), ...
                                    char(strcat(folderMat(idxFolder).name,'_NoiseMonitor_',dataSet_mapping_station.data_name(idxDataSet))), ...
                                    thirdOctaveBand)

            % LTSA highFreq
            disp('extract LTSA highFreq')
            if ~exist(fullfile(binaryPath,folderMat(idxFolder).name,'LTSA_high'), 'dir')
                mkdir(fullfile(binaryPath,folderMat(idxFolder).name,'LTSA_high'))
            end

            idx_LTSA_highFreq   = find(contains({fileMat.name},'LTSA_Long_Term_Spectral_Average_highFreq_LTSA'));

            for idxLTSA = 1:length(idx_LTSA_highFreq)
                copyfile(fullfile(  fileMat(idx_LTSA_highFreq(idxLTSA)).folder, ...
                                    fileMat(idx_LTSA_highFreq(idxLTSA)).name), ...
                         fullfile(binaryPath,folderMat(idxFolder).name,'LTSA_high'));
            end

            process_LTSA(   fullfile(binaryPath,folderMat(idxFolder).name,'LTSA_high'), ...
                            fullfile(workspacePath,'LTSA_highFreq'), ...
                            fullfile(figurePath,'LTSA_highFreq'), ...
                            char(strcat(folderMat(idxFolder).name,'_LTSA_highFreq_',dataSet_mapping_station.data_name(idxDataSet))), ...
                            hsens, ...
                            vp2p, ...
                            gain)

            % LTSA lowFreq
            disp('extract LTSA lowFreq')
            if ~exist(fullfile(binaryPath,folderMat(idxFolder).name,'LTSA_low'), 'dir')
                mkdir(fullfile(binaryPath,folderMat(idxFolder).name,'LTSA_low'))
            end

            idx_LTSA_lowFreq    = find(contains({fileMat.name},'LTSA_Long_Term_Spectral_Average_lowFreq_LTSA'));

            for idxLTSA = 1:length(idx_LTSA_lowFreq)
                copyfile(fullfile(  fileMat(idx_LTSA_lowFreq(idxLTSA)).folder, ...
                                    fileMat(idx_LTSA_lowFreq(idxLTSA)).name), ...
                         fullfile(binaryPath,folderMat(idxFolder).name,'LTSA_low'));
            end

            process_LTSA(   fullfile(binaryPath,folderMat(idxFolder).name,'LTSA_low'), ...
                            fullfile(workspacePath,'LTSA_lowFreq'), ...
                            fullfile(figurePath,'LTSA_lowFreq'), ...
                            char(strcat(folderMat(idxFolder).name,'_LTSA_lowFreq_',dataSet_mapping_station.data_name(idxDataSet))), ...
                            hsens, ...
                            vp2p, ...
                            gain)
        end
    end
end