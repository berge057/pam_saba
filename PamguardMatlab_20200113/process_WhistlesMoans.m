function process_WhistlesMoans(   fileMat, ...
                                                                    fileMappingTab, ...
                                                                    sampleRate, ...
                                                                    fftlength, ...
                                                                    binaryName, ...
                                                                    fileName, ...
                                                                    alias, ...
                                                                    workspaceSave, ...
                                                                    selTabSave)

idxWhistle = find(contains({fileMat.name},binaryName));
    
fileMat    = fileMat(idxWhistle);
    
fileMappingTab.filt = ones(size(fileMappingTab,1),1);
matData = [];
count = 0;
    
for idxFile = 1:length(fileMat)
	[pgdata,~]=loadPamguardBinaryFile(  fullfile(fileMat(idxFile).folder, ...
                                        fileMat(idxFile).name));
                                        
	dateVar = split(fileMat(idxFile).name, strcat(binaryName,'_'));
	dateVar = split(dateVar(2),'.pgdf');
	dateVar = dateVar(1);
        
	dateStr = split(dateVar,'_');
	timeStr = char(dateStr(2));
	dateStr = char(dateStr(1));
        
	dateVec = [ str2double(dateStr(1:4)), ...
                    str2double(dateStr(5:6)), ...
                    str2double(dateStr(7:8)), ...
                    str2double(timeStr(1:2)), ...
                    str2double(timeStr(3:4)), ...
                    str2double(timeStr(5:6))];
                
	if ~isempty(pgdata)
        for idxTemp = 1:length(pgdata)
            pgdata(idxTemp).startFreq   = pgdata(idxTemp).freqLimits(1);
            pgdata(idxTemp).maxWidth    = max(pgdata(idxTemp).contWidth*sampleRate/fftlength);
            pgdata(idxTemp).T           = pgdata(idxTemp).sampleDuration/sampleRate;
            pgdata(idxTemp).deltaF      = diff(pgdata(idxTemp).freqLimits);
        end
%                   = cellfun(@diff,{pgdata.freqLimits});

%         pgdata = pgdata(    [pgdata.date] > (min([pgdata.date])+1/24/60/60*60*2) & ... % filter out first 2 minutes of each recording 
%                             [pgdata.T] >= 0.1 & ... % duration of at least 0.1 second
%                             [pgdata.deltaF] >= 100); % frequency delta of at least 100 Hz
                        
        pgdata = pgdata(    [pgdata.date] > (min([pgdata.date])+1/24/60/60*60*2) & ... % filter out first 2 minutes of each recording 
                            [pgdata.T] >= 0.1 & ... % duration of at least 0.1 second
                            [pgdata.startFreq] >= 100); % frequency delta of at least 100 Hz

        selTab = table( 'Size', ...
                        [length(pgdata) 4], ...
                        'VariableNames', ...
                        {'Begin Time','End Time','Low Frequency', 'High Frequency'}, ...
                        'VariableTypes', ...
                        {'double','double', 'double', 'double'});
        
        fileMappingTab.filt(strcmp(dateVar,fileMappingTab.date_str))        = 0;
        fileMappingTab.N_slices(strcmp(dateVar,fileMappingTab.date_str))    = sum([pgdata.nSlices]);
        fileMappingTab.N_contours(strcmp(dateVar,fileMappingTab.date_str))  = length(pgdata);
                        
        for idxContour = 1:length(pgdata)
            count = count + 1;
            matData(count).contour      = pgdata(idxContour).contour*sampleRate/fftlength;
            matData(count).contWidth    = pgdata(idxContour).maxWidth;
            matData(count).date         = pgdata(idxContour).date;
            matData(count).T            = pgdata(idxContour).T;
            matData(count).nSlices      = pgdata(idxContour).nSlices;
            matData(count).amplitude    = pgdata(idxContour).amplitude;
            matData(count).freqStart    = pgdata(idxContour).freqLimits(1);
            matData(count).freqEnd      = pgdata(idxContour).freqLimits(2);
            
%             [ dB ] = rawamplitudedB( pgdata(idxContour).amplitude, 5, -165, 6);
            %             hold on
        %                 plot(pgdata(idxTone).contour*samplerate/fftlength*1e-3)

                        % fill in selection table
            selTab.("Low Frequency")(idxContour)    = pgdata(idxContour).freqLimits(1);
            selTab.("High Frequency")(idxContour)   = pgdata(idxContour).freqLimits(2);
            selTab.("Begin Time")(idxContour)       = pgdata(idxContour).startSample/sampleRate;
            selTab.("End Time")(idxContour)         = pgdata(idxContour).startSample/sampleRate+pgdata(idxContour).sampleDuration/sampleRate;
        end
        tempName = strsplit(char(fileMappingTab.file_name(strcmp(dateVar,fileMappingTab.date_str))),'.wav');
        tempName = char(tempName(1));
        if(size(selTab,1) ~= 0 && ~isempty(tempName))
            writetable( selTab, ...
                        char(fullfile(   selTabSave, ...
                                    strcat(tempName,'_',alias,'.txt'))), ...
                        'Delimiter','\t')
        end
    end


end

save(char(fullfile(workspaceSave,strcat(fileName,'.mat'))),'matData')

fileMappingTab = fileMappingTab(fileMappingTab.filt == 0,:);

end