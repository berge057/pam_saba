function process_NoiseMonitor(  fileMat, ...
                                binaryName, ...
                                workspaceSave, ...
                                fileName, ...
                                thirdOctaveBand)

idxNoiseMonitor = find(contains({fileMat.name},binaryName));
    
fileMat    = fileMat(idxNoiseMonitor);
    
noiseMat1 = zeros(size(thirdOctaveBand,1),0);
noiseMat2 = zeros(size(thirdOctaveBand,1),0);
matNoiseMonitor = [];
    
for idxFile = 1:length(fileMat)
	[pgdata,~]=loadPamguardBinaryFile(  fullfile(fileMat(idxFile).folder, ...
                                        fileMat(idxFile).name));
	if ~isempty(pgdata)        
        if mean([pgdata.nBands]) == size(thirdOctaveBand,1)
            if idxFile == 1
                matNoiseMonitor = pgdata;
            else
                matNoiseMonitor = [matNoiseMonitor pgdata];
            end
            for idxEntry = 1:length(pgdata)
                noiseMat1 = [noiseMat1; pgdata(idxEntry).noise(1,:)];
                noiseMat2 = [noiseMat2; pgdata(idxEntry).noise(2,:)];
            end
        end
    end
end

save(char(fullfile(workspaceSave,strcat(fileName,'.mat'))),'matNoiseMonitor','noiseMat1','noiseMat2')
end