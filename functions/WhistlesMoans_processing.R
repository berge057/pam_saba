WhistlesMoans_processing <- function(WhistlesMoansMat,lat_station,lon_station){
  detect_min   <- WhistlesMoansMat
  detect_hour  <- WhistlesMoansMat
  detect_day   <- WhistlesMoansMat
  
  #####################################
  # break down data into minutes
  #####################################
  detect_min$dateStr      <- matlab2POS(detect_min$date)
  detect_min$time_minute  <- as.POSIXct(cut(detect_min$dateStr, breaks = "1 min"),tz='UTC')
  
  detect_min  <-    detect_min %>% 
                    group_by(time_minute,dataSet,SN)%>%
                    summarize(  count = n())
  
  detect_min$time_hour    <- as.POSIXct(cut(as.POSIXct(detect_min$time_minute), "hour"),tz='UTC')
  detect_min$time_day     <- as.Date(cut(as.POSIXct(detect_min$time_minute), "day"))
  
  #####################################
  # break down data into hours
  #####################################
  minHour   <- min(as.POSIXct(cut(as.POSIXct(matlab2POS(WhistlesMoansMat$date)), "hour"),tz='UTC'))
  maxHour   <- max(as.POSIXct(cut(as.POSIXct(matlab2POS(WhistlesMoansMat$date)), "hour"),tz='UTC'))
  
  seqHour <- seq( from=minHour,
                  to=maxHour,by="hour")
  
  detect_hour$dateStr     <- matlab2POS(detect_hour$date)
  detect_hour$time_hour   <- as.POSIXct(cut(detect_hour$dateStr, breaks = "hour"),tz='UTC')
  
  detect_hour            <-  detect_hour %>% 
                              group_by(time_hour,dataSet,SN) %>% 
                              summarize(  count = n())
  
  detect_hour$pos_minutes <- 0

  idxFill <- which(!(seqHour %in% detect_hour$time_hour))
  
  dfFill  <- data.frame(matrix(ncol = 5, nrow = length(idxFill)))
  colnames(dfFill) <- colnames(detect_hour)
  dfFill$time_hour <- seqHour[idxFill]
  
  detect_hour <- rbind(detect_hour,dfFill)
  detect_hour <- detect_hour[order(detect_hour$time_hour),]
  detect_hour <- detect_hour %>% ungroup %>% fill(dataSet,SN,.direction='downup')

  uniquePosHour <- unique(detect_hour$time_hour)
  
  for(idxHour in uniquePosHour){
    detect_hour$pos_minutes[detect_hour$time_hour == idxHour] <- length(which(detect_min$time_hour == idxHour))
  }
  
  detect_hour$day          <- as.Date(detect_hour$time_hour)
  detect_hour$hour         <- hour(detect_hour$time_hour)
  detect_hour$month_year   <- paste0(month(detect_hour$time_hour),'/',year(detect_hour$time_hour))
  
  # plot daily positive minutes
  mySunlightTimes <- getSunlightTimes(date = as.Date(detect_hour$day), 
                                      lat = lat_station, 
                                      lon = lon_station, tz = "UTC")
  
  detect_hour$hourSunset    <- hour(mySunlightTimes$sunset)+minute(mySunlightTimes$sunset)/60+second(mySunlightTimes$sunset)/60/60
  detect_hour$hourSunrise   <- hour(mySunlightTimes$sunrise)+minute(mySunlightTimes$sunrise)/60+second(mySunlightTimes$sunrise)/60/60
  
  #####################################
  # break down data into days
  #####################################
  minDay   <- min(as.POSIXct(cut(as.POSIXct(matlab2POS(WhistlesMoansMat$date)), "day"),tz='UTC'))
  maxDay   <- max(as.POSIXct(cut(as.POSIXct(matlab2POS(WhistlesMoansMat$date)), "day"),tz='UTC'))

  seqDay <- seq.Date(from=as.Date(minHour),
                     to=as.Date(maxHour),by="day")

  detect_day$dateStr     <- matlab2POS(detect_day$date)
  detect_day$time_day    <- as.Date(cut(detect_day$dateStr, breaks = "day"))

  detect_day             <-  detect_day %>% 
                              group_by(time_day,dataSet,SN) %>% 
                              summarize(  count = n())
  
  detect_day$pos_minutes  <- 0
  detect_day$pos_hours    <- 0
  
  idxFill <- which(!(seqDay %in% as.Date(detect_day$time_day)))
  
  dfFill  <- data.frame(matrix(ncol = 6, nrow = length(idxFill)))
  colnames(dfFill) <- colnames(detect_day)
  dfFill$time_day <- seqDay[idxFill]
  
  detect_day <- rbind(detect_day,dfFill)
  detect_day <- detect_day[order(detect_day$time_day),]
  detect_day <- detect_day %>% ungroup %>% fill(dataSet,SN,.direction='downup')
  
  uniquePosDay <- unique(detect_day$time_day)
  
  for(idxDay in uniquePosDay){
    detect_day$pos_minutes[detect_day$time_day == idxDay] <- length(which(detect_min$time_day == idxDay))
    detect_day$pos_hours[detect_day$time_day == idxDay]   <- length(which(detect_hour$day == idxDay))
  }
  
  detect_day$day          <- as.POSIXct(format(detect_day$time_day,'%Y-%m-%d 00:00:00'),tz='UTC')
  detect_day$hour         <- hour(detect_day$time_day)
  detect_day$month        <- month(detect_day$time_day)
  detect_day$year         <- year(detect_day$time_day)
  detect_day$month_year   <- cut(as.POSIXct(detect_day$day), "month")
  
  return(list(detect_day=detect_day,
              detect_hour=detect_hour,
              detect_min=detect_min))
}