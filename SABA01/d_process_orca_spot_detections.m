clear all
close all
clc

mainPath = fullfile(pwd);

rawPath     = fullfile(mainPath,'raw');
selecPath   = fullfile(mainPath,'selections');
dailyPath     = fullfile(mainPath,'daily');

settings_detector = '0.85_2.0s_0.5s';

rawMat = dir(rawPath);
rawMat = rawMat(3:end);
rawMat = rawMat([rawMat.isdir]);

dayStruct = struct();
count_days = 1;

for idxFolder = 1:length(rawMat)
    source = dir(fullfile(  rawMat(idxFolder).folder, ...
                            rawMat(idxFolder).name, ...
                            settings_detector, ...
                            'annotation_result.txt'));
	if length(source) == 1
        source = fullfile(source.folder,source.name);

        % read selection file
        selecTab = readtable(fullfile(  source));

        % copy selection file
    % 	length(source)

        destination = fullfile( selecPath, ...
                                settings_detector, ...
                                strcat(rawMat(idxFolder).name,'.txt'));

        copyfile(source,destination)

        % compute daily data
        dateTimeFile    = split(rawMat(idxFolder).name,'.');
        dateTimeFile    = dateTimeFile(3);
        dateFile        = split(dateTimeFile,'T');
        timeFile        = dateFile(2);
        timeFile        = split(timeFile,'Z');
        timeFile        = char(timeFile(1));
        dateFile        = char(dateFile(1));

        dateTimeFileVec = [ str2double(dateFile(1:4)) ...
                            str2double(dateFile(5:6)) ...
                            str2double(dateFile(7:8)) ...
                            str2double(timeFile(1:2)) ...
                            str2double(timeFile(3:4)) ...
                            str2double(timeFile(5:6))];

        selecTab.BeginDatenum   = selecTab.BeginTime_s_/86400+datenum(dateTimeFileVec);
        selecTab.EndDatenum     = selecTab.EndTime_s_/86400+datenum(dateTimeFileVec);

        selecTab.day   = floor(selecTab.BeginDatenum);

        % initialise for first index in the loop
        if idxFolder == 1
            currentDay = min(selecTab.day);

            idxFiltCurrent  = selecTab.day == currentDay;
            idxFiltNext     = selecTab.day == (currentDay+1);
            
            dayStruct.startDate = reshape(  selecTab.BeginDatenum(idxFiltCurrent), ...
                                                length(find(idxFiltCurrent)),1);
            dayStruct.endDate   = reshape(  selecTab.EndDatenum(idxFiltCurrent), ...
                                                length(find(idxFiltCurrent)),1);
            dayStruct.duration  = reshape(  (selecTab.EndDatenum(idxFiltCurrent)-selecTab.BeginDatenum(idxFiltCurrent))*24*60*60, ...
                                                length(find(idxFiltCurrent)),1);
            dayStruct.ID        = reshape(  selecTab.SoundType(idxFiltCurrent), ...
                                                length(find(idxFiltCurrent)),1);

            if any(idxFiltNext)
                dayStruct(count_days).N = length(dayStruct(count_days).ID);
                dayStruct(count_days).day = currentDay;
                count_days = count_days + 1;
                initialize_next
                currentDay = max(selecTab.day);
            end
        % if not the first in the loop, concatenate in structure
        else
            if currentDay == min(selecTab.day)
                idxFiltCurrent  = selecTab.day == currentDay;
                idxFiltNext     = selecTab.day == (currentDay+1);

                dayStruct(count_days).startDate = [ dayStruct(count_days).startDate; ...
                                        reshape(    selecTab.BeginDatenum(idxFiltCurrent), ...
                                                    length(find(idxFiltCurrent)),1)];
                dayStruct(count_days).endDate   = [ dayStruct(count_days).endDate; ...
                                        reshape(    selecTab.EndDatenum(idxFiltCurrent), ...
                                                    length(find(idxFiltCurrent)),1)];
                dayStruct(count_days).duration  = [ dayStruct(count_days).duration; ...
                                        reshape(    (selecTab.EndDatenum(idxFiltCurrent)-selecTab.BeginDatenum(idxFiltCurrent))*24*60*60, ...
                                                    length(find(idxFiltCurrent)),1)];
                dayStruct(count_days).ID        = [ dayStruct(count_days).ID; ...
                                                    reshape(    selecTab.SoundType(idxFiltCurrent), ...
                                                                length(find(idxFiltCurrent)),1)];

                if any(idxFiltNext)
                    dayStruct(count_days).N = length(dayStruct(count_days).ID);
                    dayStruct(count_days).day = currentDay;
                    count_days = count_days + 1;
                    initialize_next
                    currentDay = max(selecTab.day);
                end
            % if gap in dates, save object and start a new current day.
            else
                dayStruct(count_days).N = length(dayStruct(count_days).ID);
                dayStruct(count_days).day = currentDay;
                count_days = count_days + 1;
                currentDay = min(selecTab.day);
                
                idxFiltCurrent  = selecTab.day == currentDay;
                idxFiltNext     = selecTab.day == (currentDay+1);
                initialize_next

                if any(idxFiltNext)
                    dayStruct(count_days).N = length(dayStruct(count_days).ID);
                    dayStruct(count_days).day = currentDay;
                    count_days = count_days + 1;
                    initialize_next
                    currentDay = max(selecTab.day);
                end
            end
        end
    else
        delete(fullfile(rawMat(idxFolder).folder,rawMat(idxFolder).name))
    end
end

save(fullfile(dailyPath,'SN276_orca_spot'),'dayStruct')

%%

% contourTab_201604_201608 = readtable('file_inventory_all_201604_201608.csv');
% contourTab_201612_201705 = readtable('file_inventory_all_201612_201705.csv');
% contourTab_201705_201710 = readtable('file_inventory_all_201705_201710.csv');
% contourTab_201712_201804 = readtable('file_inventory_all_201712_201804.csv');
% contourTab_201804_201808 = readtable('file_inventory_all_201804_201808.csv');
% contourTab_201812_201904 = readtable('file_inventory_all_201812_201904.csv');
% 
% contourTab_all = [  contourTab_201604_201608; ...
%                     contourTab_201612_201705; ...
%                     contourTab_201705_201710; ...
%                     contourTab_201712_201804; ...
%                     contourTab_201804_201808; ...
%                     contourTab_201812_201904];
% 
% plot(   contourTab_all.start_time,  (contourTab_all.N_contours_lowFreq+1), ...
%         contourTab_all.start_time,  (contourTab_all.N_contours_highFreq+1))
% 
% dateFormat = 2;
% datetick('x',dateFormat)