clear all
close all
clc

%% define path and read file table
mainPath        = pwd;
selecTabPath = fullfile(pwd,'ground_truth');

fileTab = readtable('file_inventory_all.csv');

%%  read selection tables
selectTabMat = dir(selecTabPath);
selectTabMat = selectTabMat([selectTabMat.isdir]==0);
% dirMat = dirMat(3:end);

h = waitbar(0,'Please wait...');
for idxSelectTab = 1:length(selectTabMat)
    waitbar(idxSelectTab/length(selectTabMat),h)
    selectTab = readtable(fullfile(selectTabMat(idxSelectTab).folder,selectTabMat(idxSelectTab).name),'ReadVariableNames', true,'Delimiter','\t');
    
    if idxSelectTab == 1
        selectTabAll = selectTab;
    else
        selectTabAll = [selectTabAll; selectTab];
    end
end
close(h)

selectTabAll.ID(strcmp(selectTabAll.ID,'possibly_HB'))      = {'possibly-HB'};
selectTabAll.ID(strcmp(selectTabAll.ID,'clicks_LF'))        = {'clicks-LF'};
selectTabAll.ID(strcmp(selectTabAll.ID,'possibly_fish'))    = {'possibly-fish'};

writetable(selectTabAll,fullfile(mainPath,'SN276_checked_selection.txt'),'Delimiter','\t')

%% tidy up table of files to choose
dataSetUnique = unique(fileTab.data_set);

% keep only tapes with no more than 500 slices
fileTab = fileTab(fileTab.N_slices_lowFreq < 500,:);

% trim first and last 100 files for each data set
for idx = 1:length(dataSetUnique)
    idxFilt = find(strcmp(fileTab.data_set,dataSetUnique(idx)));
    idxFilt = idxFilt([1:100 (length(idxFilt)-100):length(idxFilt)]);
    fileTab(idxFilt,:) = [];
end

%% calculate the number of clips to produce
N_signal    = length(find(strcmp(selectTabAll.ID,'HB')))+length(find(strcmp(selectTabAll.ID,'possibly-HB')));
N_noise     = size(selectTabAll,1)-(length(find(strcmp(selectTabAll.ID,'HB')))+length(find(strcmp(selectTabAll.ID,'possibly-HB'))));

N_target = N_signal*3; % we target 3 times more noise clips than signal clips

N_autoNoise = N_target-N_noise;
N_per_dataSet = ceil(N_autoNoise/length(dataSetUnique));
N_clips_per_file = 20;%ceil(N_clips/size(fileTab,1));
N_files_per_dataSet = ceil(N_per_dataSet/N_clips_per_file);

% draw the files to use in each data set
for idx = 1:length(dataSetUnique)
    idxFilt = find(strcmp(fileTab.data_set,dataSetUnique(idx)));
    p       = randperm(length(idxFilt)); % random permutation
    idxFilt = idxFilt(setdiff(1:length(idxFilt),p(1:N_files_per_dataSet)));
    fileTab(idxFilt,:) = [];
end


%% create selection table for auto noise clips
N_clips = N_clips_per_file*size(fileTab,1);
min_max_samples = [1 5]; % range of sample length to be picked
% random draw for the length of the clips
clip_length = (min_max_samples(2)-min_max_samples(1)).*rand(N_clips,1) + min_max_samples(1);
N_crop = 5000;


selTab = table( 'Size', ...
                [N_clips 11], ...
                'VariableNames', ...
                {   'Selection', ...
                    'View', ...
                    'Channel', ...
                    'Begin Time (s)', ...
                    'End Time (s)', ...
                    'Low Freq (Hz)', ...
                    'High Freq (Hz)', ...
                    'Begin File', ...
                    'Begin Date Time', ...
                    'Begin Date', ...
                    'ID'}, ...
                'VariableTypes', ...
                {   'double', ...
                    'cellstr', ...
                    'double', ...
                    'double', ...
                    'double', ...
                    'double', ...
                    'double', ...
                    'cellstr', ...
                    'cellstr', ...
                    'cellstr', ...
                    'cellstr'});
                
h1 = waitbar(0,'Please wait... clip selection');
count_sel = 0;
for idxFile = 1:size(fileTab,1)
    waitbar(idxFile/size(fileTab,1),h1)
    
    info = audioinfo(char(fullfile( fileTab.folder_name(idxFile), ...
                                    fileTab.file_name(idxFile))));
                                
    dateTimeFile    = split(fileTab.file_name(idxFile),'.');
    dateTimeFile    = dateTimeFile(3);
    dateFile        = split(dateTimeFile,'T');
    timeFile        = dateFile(2);
    timeFile        = split(timeFile,'Z');
    timeFile        = char(timeFile(1));
    dateFile        = char(dateFile(1));
    
    dateFileVec = [ str2double(dateFile(1:4)) ...
                    str2double(dateFile(5:6)) ...
                    str2double(dateFile(7:8)) ...
                    str2double(timeFile(1:2)) ...
                    str2double(timeFile(3:4)) ...
                    str2double(timeFile(5:6))];
	
	for idxClip = 1:N_clips_per_file  
        count_sel = count_sel + 1;
        N_samples_clip = round(clip_length(count_sel)/(1/info.SampleRate));

        % random draw for the start of the clip
        max_loc = info.TotalSamples-(N_samples_clip+N_crop); % crop the end of the file
        min_loc = N_crop; % crop the end of the file
        start_loc = round((max_loc-min_loc).*rand(1,1) + min_loc);
        
        % find start of clip
        dateFileVecNow = datenum(dateFileVec)+start_loc*1/info.SampleRate/60/60/24;
        
        % fill in selection table
        selTab.("Begin File")(count_sel) = {fileTab.file_name(idxFile)};
        selTab.("Begin Date")(count_sel) = {datestr(dateFileVecNow,'yyyy/mm/dd')};
        selTab.("Begin Date Time")(count_sel) = {datestr(dateFileVecNow,'yyyy/mm/dd HH:MM:SS.FFF')};
        selTab.("Begin Time (s)")(count_sel) = start_loc*1/info.SampleRate;
        selTab.("End Time (s)")(count_sel) = (start_loc+N_samples_clip)*1/info.SampleRate;
        selTab.("Low Freq (Hz)")(count_sel) = 0;
        selTab.("High Freq (Hz)")(count_sel) = 0;
        selTab.Channel(count_sel) = 1;
        selTab.ID(count_sel) = {'noise-auto'};
        selTab.Selection(count_sel) = count_sel;
        selTab.View(count_sel) = {'none'};
        
        dateFileFormat = datestr(dateFileVecNow,'yyyymmdd.HHMMSS.FFF');
        dateFileFormat = dateFileFormat(1:end-1); % take only the first 2 digits for the milliseconds
    end
end
close(h1)

selTab = selTab(randperm(size(selTab,1)),:);
selTab = selTab(1:N_autoNoise,:);


writetable(selTab,fullfile(mainPath,'SN276_auto_noise_selection.txt'),'Delimiter','\t')