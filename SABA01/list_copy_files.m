clear all
close all
clc

fileInv = readtable('file_inventory_all.csv','Delimiter',',');

auto_noise_tab  = readtable('SN276_auto_noise_selection.txt');
checked_tab     = readtable('SN276_checked_selection.txt');

fileList = [auto_noise_tab.Var8; checked_tab.Var9];

uniqueFiles = unique(fileList);


tabCopy = fileInv(ismember(fileInv.file_name,uniqueFiles),:);

writetable(tabCopy,'tabCopy.csv','Delimiter',',')