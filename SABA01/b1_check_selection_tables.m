clear all
close all
clc

mainPath        = pwd;
selecTabPath    = fullfile(mainPath,'ground_truth');
outputPath      = fullfile(mainPath,'orca_spot','output');

selectTabMat = dir(selecTabPath);
selectTabMat = selectTabMat([selectTabMat.isdir]==0);
% dirMat = dirMat(3:end);

for idxSelectTab = 1:length(selectTabMat)
    selectTab = readtable(fullfile(selectTabMat(idxSelectTab).folder,selectTabMat(idxSelectTab).name),'ReadVariableNames', true,'Delimiter','\t');
    
    selectTabMat(idxSelectTab).name
    
    for idx = 1:size(selectTab,1)
        idxDiscrepency = find(  ((selectTab.BeginTime_s_(idx) < selectTab.BeginTime_s_(setdiff(1:end,idx))) & ...
                                (selectTab.EndTime_s_(idx) > selectTab.BeginTime_s_(setdiff(1:end,idx)))) | ...
                                ((selectTab.BeginTime_s_(idx) < selectTab.EndTime_s_(setdiff(1:end,idx))) & ...
                                (selectTab.EndTime_s_(idx) > selectTab.EndTime_s_(setdiff(1:end,idx)))));
        if length(idxDiscrepency~=0)
            selectTab.Selection(idx)
            a = selectTab(setdiff(1:end,idx),:);
            a.Selection(idxDiscrepency)
            idx
            idxDiscrepency
            return
        end
    end
    
    if idxSelectTab == 1
        selectTabAll = selectTab;
    else
        selectTabAll = [selectTabAll; selectTab];
    end
end