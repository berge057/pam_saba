# script that derives the HERAS index directly from the StoX projects.

rm(list=ls())

library(tidyverse)
library(data.table)
library(ggplot2)
library(lubridate)
library(suncalc)
library(viridis)

#Set up directories

#path <- 'J:/git/HERAS/'
path <- 'J:/PAM_saba'

try(setwd(path),silent=TRUE)

# setup paths and source functions
dataPath        <- file.path(".",'data')
figuresPath     <- file.path(".",'figures')
functionPath    <- file.path(".",'functions')

PAM_SABA_overview <- read.csv(file.path(dataPath,'PAM_SABA_overview.csv'))

detector_name <- 'HB_20200830'

threshold <- 0.85

# define variables
#stationMat <- unique(PAM_SABA_overview$station_ID)

stationMat <- 'SABA01' # orca_spot only ran for this station

scaling_factor <- 1.5

# loop on stations

idxStation <- 1

station <- stationMat[idxStation]

fileInventory                   <- read.csv(file.path(".",station,paste0(station,'_file_inventory_all.csv')))
fileInventory$orca_spot_dataSet <- 0
fileInventory$orca_spot         <- 0

stationOverview   <- PAM_SABA_overview[PAM_SABA_overview$station_ID == station,]
lat_station       <- stationOverview$latitude[1]
lon_station       <- stationOverview$longitude[1]

outputPath <- file.path(".",station,'orca_spot',detector_name,'raw_ouputs')
resultsPath <- file.path(".",station,'orca_spot',detector_name)

dataSetsDir <- list.dirs(path = outputPath, full.names = TRUE, recursive = F)

# loop on data set
for(idxDataSets in 1:length(dataSetsDir)){
  dataSet <- unlist(strsplit(dataSetsDir[idxDataSets],'/'))
  dataSet <- dataSet[length(dataSet)]
  
  fileInventory$orca_spot_dataSet[fileInventory$data_name == dataSet] <- 1

  fileDir <- list.dirs(path = file.path(dataSetsDir[idxDataSets]), full.names = TRUE, recursive = F)
  
  # loop on tape results
  for(idxDir in 1:length(fileDir)){
    print(idxDir/length(fileDir))
    
    # find file name
    fileName <- unlist(strsplit(fileDir[idxDir],'/'))
    fileName <- fileName[length(fileName)]
    dateTimeFile <- unlist(strsplit(fileName,'[.]'))
    dateTimeFile <- dateTimeFile[length(dateTimeFile)]
    
    dateTimeFile <- as.POSIXct(dateTimeFile,format = "%Y%m%dT%H%M%S",tz='UTC')
    
    idxFile <- which(fileInventory$file_name == paste0(fileName,'.wav'))
    
    # read orca_spot output
    output.file <- list.files(path = fileDir[idxDir],pattern = "\\.output$",recursive = TRUE)
    
    # process output
    con=file(file.path(fileDir[idxDir],output.file),open="r")
    lines <- readLines(con)
    
    # find the start of parameter 2
    out <- grep(pattern='|I|time', lines,value=TRUE,fixed = TRUE)
    
    if(length(out) != 0){
      HB_orca_spot <- as.data.frame(array(data = NA,dim = c(length(out),4)))
      colnames(HB_orca_spot) <- c('time_start','time_end','prob','pred')
      
      for(idxLine in 1:length(out)){
        #print(idxLine/length(out))
        splitLine <- unlist(strsplit(out[idxLine],split=','))
        
        # fill in time
        timeStr <- unlist(strsplit(splitLine[1],split='time='))
        timeStr <- timeStr[2]
        timeStr <- unlist(strsplit(timeStr,split='-'))
        
        HB_orca_spot$dateTimeFile         <- dateTimeFile
        
        HB_orca_spot$time_start[idxLine]  <- as.numeric(timeStr[1])
        HB_orca_spot$time_end[idxLine]    <- as.numeric(timeStr[2])
        
        HB_orca_spot$time_start_str[idxLine] <- as.POSIXct(as.numeric(dateTimeFile)+HB_orca_spot$time_start[idxLine],origin = "1970-01-01",tz='UTC')
        HB_orca_spot$time_end_str[idxLine]   <- as.POSIXct(as.numeric(dateTimeFile)+HB_orca_spot$time_end[idxLine],origin = "1970-01-01",tz='UTC')
        
        # fill in probability
        prob <- unlist(strsplit(splitLine[3],split='prob='))
        HB_orca_spot$prob[idxLine]  <- as.numeric(prob[length(prob)])
        
        # fill in prediction
        if(HB_orca_spot$prob[idxLine] > threshold){
          HB_orca_spot$pred[idxLine] <- 'HB'
        }else{
          HB_orca_spot$pred[idxLine] <- 'noise'
        }
      }

      HB_orca_spot <- HB_orca_spot[HB_orca_spot$pred == 'HB',]
      
      if(idxDir == 1){
        HB_orca_spot_all <- HB_orca_spot
      }else{
        HB_orca_spot_all <- rbind(HB_orca_spot_all,HB_orca_spot)
      }
      fileInventory$orca_spot[idxFile] <- 1
    }
    close(con)
  }
  write.csv(file = file.path(resultsPath,paste0(dataSet,'_detections_orca_spot.csv')),x=HB_orca_spot_all,row.names = F)
}

write.csv(file = file.path(dataPath,'missing_results.csv'),x=fileInventory[  fileInventory$orca_spot_dataSet == 1 & 
                fileInventory$orca_spot == 0,],row.names = F)
